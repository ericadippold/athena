# Athena Dashboard Project

## Getting Started
1. Clone the repo
2. In the terminal, navigate to the athena folder
3. In your terminal, navigate into the athena-dashboard folder
3. Run npm install
4. Open the project in your IDE
5. Run npm start
6. Check out the app at http://localhost:3000/

## Navigating the App
- Toggle: On the main page, you will be able to switch from viewing the social security card and the regular cards with the toggle.
- Incrementor / Decrementor: On the main page, you will be able to increment and decrement the amount of cards visable by using the increment / decrement cards button.
