import React from "react";
import "./CardStyles.css";
import blueLine from "../assets/blue-line.png";
import buttonBg from "../assets/button-bg.svg";

export default function LargeCard({ cards }) {
  return (
    <div className="large-card-container">
      {cards && (
        <div>
          <img className="lg-blue-line-img" src={blueLine} alt="lg-blue-line" />
          <div className="lg-content-container">
            <h1>HEADER</h1>
            <div className="inner-content-container">
              <div className="lg-inner-flex-content">
                <div className="subtitle">{cards[0].subtitle}</div>
                <div className="lg-content-box-1">
                  <div className="content-body">{cards[0].content}</div>
                </div>
              </div>
              <div className="lg-inner-flex-content">
                <div className="subtitle">{cards[1].subtitle}</div>
                <div className="lg-content-box-2">
                  <div className="content-body">{cards[1].content}</div>
                </div>
              </div>
              <div className="lg-inner-flex-content">
                <div className="subtitle">{cards[2].subtitle}</div>
                <div className="lg-content-box-2">
                  <div className="content-body">{cards[2].content}</div>
                </div>
              </div>
            </div>
          </div>
          <button className="lg-button-container">
            <img className="lg-button-background" src={buttonBg} />
            <div className="lg-button-text">BUTTON TEXT</div>
          </button>
        </div>
      )}
    </div>
  );
}
