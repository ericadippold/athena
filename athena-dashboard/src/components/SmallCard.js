import React from "react";
import "./CardStyles.css";
import blueLine from "../assets/blue-line.png";
import buttonBg from "../assets/button-bg.svg";

export default function SmallCard({card}) {
  return (
    <div className="small-card-container">
      <img className="sm-blue-line-img" src={blueLine} alt="sm-blue-line" />
      <div className="sm-content-container">
        <h1>HEADER</h1>
        <div className="subtitle">{card.subtitle}</div>
        <div className="sm-content-box">
          <div className="content-body">{card.content}</div>
        </div>
        <button className="sm-button-container">
          <img src={buttonBg} />
          <div className="button-text">BUTTON TEXT</div>
        </button>
      </div>
    </div>
  );
}
