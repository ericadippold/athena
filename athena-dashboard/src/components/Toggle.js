import "./Toggle.css";
export default function Toggle({ toggle, setToggle }) {

  const handleToggleClick = () => {
    setToggle(!toggle);
  };


  return (
    <div>
      <div>{toggle ? 'Show cards': 'Show Social Security Card'}</div>
      <label className="switch">
        <input id='checkbox' onClick={handleToggleClick} type="checkbox" />
        <span className="slider round"></span>
      </label>
    </div>
  );
}
