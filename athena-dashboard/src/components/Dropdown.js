import { Menu } from "@headlessui/react";
import "./Dropdown.css";
import dropDownArrow from "../assets/dropdown-arrow.svg";

export default function MyDropdown({ individual }) {
  return (
    <Menu as="div" className="menu-container">
      <div>
        <Menu.Button className="menu-button">
          <div className="menu-title">Choose...</div>
          <img className="dropdown-arrow" src={dropDownArrow} alt="dropdown-arrow" />
        </Menu.Button>
        <Menu.Items className="menu-items">
          <Menu.Item className="menu-item">
            <div className="text">{individual?.user_info?.full_name}</div>
          </Menu.Item>
        </Menu.Items>
      </div>
    </Menu>
  );
}
