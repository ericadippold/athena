import "./PlusMinusNum.css";
export default function PlusMinusNum({ cardCount, setCardCount }) {
  const increaseNum = () => {
    if (cardCount < 9) {
      setCardCount((num) => num + 1);
    }
  };

  const decreaseNum = () => {
    if (cardCount > 3) {
      setCardCount((num) => num - 1);
    }
  };

  return (
    <div className="wrapper">
      <span onClick={decreaseNum} className="minus">
        -
      </span>
      <span className="num">{cardCount}</span>
      <span onClick={increaseNum} className="plus">
        +
      </span>
    </div>
  );
}
