import React, { useEffect, useState } from "react";
import "./SocialSecurityStyles.css";
import MyDropdown from "./Dropdown";
import darkBlueCircle from "../assets/dark-blue-circle.svg";
import lightBlueCircle from "../assets/light-blue-circle.svg";
import donutChart from "../assets/donut-chart.svg";
import rectangle1 from "../assets/rectangle-amount-1.svg";
import rectangle2 from "../assets/rectangle-amount-2.svg";
import grayLine from "../assets/gray-line.svg";
import upArrowSquare from "../assets/ant-design-up-square.svg";
import downArrowSquare from "../assets/ant-design-down-square.svg";
import smallGrayLine from "../assets/small-gray-line.svg";

const userID = 1;

export default function SocialSecurityCard() {
  const [individual, setIndividual] = useState({});
  const [currentAge, setCurrentAge] = useState();

  useEffect(() => {
    const getUser = async () => {
      const url = `/users/${userID}`;
      fetch(url)
        .then((res) => res.json())
        .then((data) => {
          setIndividual(data);
          setCurrentAge(data.assumptions.retirement_age);
        });
    };
    getUser();
  }, [userID]);

  const increaseAge = () => {
    setCurrentAge((age) => age + 1);
  };

  const decreaseAge = () => {
    setCurrentAge((age) => age - 1);
  };

  return (
    <div className="ss-card-container">
      {currentAge && (
        <div>
          <div className="top-container">
            <div>Interaction</div>
          </div>
          <div className="content-container">
            <div className="header-content">
              <h1>Best Social Security Claimed Age</h1>
              <div>Our Recommendation</div>
            </div>
            <div className="content-container-2">
              <div className="inner-content">
                <div className="inner-text">
                  <img src={darkBlueCircle} alt="dark-blue-circle" />
                  <span>Benjamin claim in 70</span>
                </div>
                <div className="inner-text">
                  <img src={lightBlueCircle} alt="light-blue-circle" />
                  <span>Jasmine claim in 68</span>
                </div>
              </div>
              <div className="donut-chart">
                <img src={donutChart} alt="donut-chart" />
                <div className="rectangle-amount-1">
                  <img src={rectangle1} alt="rectangle-amount" />
                </div>
                <div className="rectangle-amount-2">
                  <img className="rectangle-amount-2" src={rectangle2} alt="rectangle-amount" />
                </div>
              </div>
            </div>
          </div>
          <div className="line-container">
            <img src={grayLine} alt="gray-line" />
          </div>
          <div className="content-container-3">
            <div>Household Members</div>
            <MyDropdown individual={individual} />
          </div>
          <div className="content-container-4">
            <div>
              <div className="header-text-1">
                <div>Your ideal retire age</div>
                <div className="age">
                  <div>{currentAge}</div>
                  <div className="flex-col-container">
                    <img
                      onClick={increaseAge}
                      className="img-1"
                      src={upArrowSquare}
                      alt="up-square"
                    />
                    <img
                      onClick={decreaseAge}
                      className="img-2"
                      src={downArrowSquare}
                      alt="down-square"
                    />
                  </div>
                </div>
                <div className="line-container-2">
                  <img src={smallGrayLine} alt="gray-line" />
                </div>
              </div>
            </div>
            <div className="header-text-2">
              <div>Annual Social Security Payment</div>
              <div className="amount">$18,000</div>
            </div>
          </div>
          <div className="content-container-5">
            <button className="button-1">Use Ideal {currentAge}</button>
            <button className="button-2">Accept 70</button>
          </div>
        </div>
      )}
    </div>
  );
}
