import React from "react";
import "./CardStyles.css";
import blueLine from "../assets/blue-line.png";
import buttonBg from "../assets/button-bg.svg";

export default function MediumCard({cards}) {

  return (
    <div className="medium-card-container">
      <img className="md-blue-line-img" src={blueLine} alt="md-blue-line" />
      <div className="md-content-container">
        <h1>HEADER</h1>
        <div className="inner-content-container">
          <div className="md-inner-flex-content">
            <div className="subtitle">{cards[0].subtitle}</div>
            <div className="md-content-box">
              <div className="content-body">{cards[0].content}</div>
            </div>
          </div>
          <div>
            <div className="subtitle">{cards[1].subtitle}</div>
            <div className="md-content-box-2">
              <div className="content-body">{cards[1].content}</div>
            </div>
          </div>
        </div>
      </div>
      <button className="md-button-container">
        <img className="md-button-background" src={buttonBg} />
        <div className="md-button-text">BUTTON TEXT</div>
      </button>
    </div>
  );
}
