import { render, screen } from "@testing-library/react";
import SmallCard from "./components/SmallCard";
import Toggle from "./components/Toggle";

test("renders small card", () => {
  render(<SmallCard card={{ subtitle: "subtitle 1", context: "context 1" }} />);
  const textElement = screen.getByText(/HEADER/i);
  expect(textElement);
});

test("render toggle component", () => {
  render(<Toggle />);
  expect(screen.getByText("Show Social Security Card"));
  expect(screen.getByRole("checkbox"));
});
