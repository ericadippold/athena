import React, { useEffect, useState } from "react";
import "./Home.css";
import LargeCard from "./components/LargeCard";
import MediumCard from "./components/MediumCard";
import SmallCard from "./components/SmallCard";
import SocialSecurityCard from "./components/SocialSecurityCard";
import Toggle from "./components/Toggle";
import PlusMinusNum from "./components/PlusMinusNum";

export default function Home() {
  const [cardData, setCardData] = useState();
  const [toggle, setToggle] = useState(true);
  const [cardCount, setCardCount] = useState(9);

  useEffect(() => {
    let currentCards = [];
    for (let i = 0; i < cardCount; i++) {
      currentCards.push({ subtitle: `SUBTITLE ${i + 1}`, content: `CONTENT ${i + 1}` });
    }
    setCardData(currentCards);
  }, [cardCount]);

  return (
    <div className="page-layout">
      <div className="toggle-container">
        <Toggle toggle={toggle} setToggle={setToggle} />
      </div>
      {!toggle ? (
        <div>
          <span>Increment / Decrement Cards</span>
          <PlusMinusNum cardCount={cardCount} setCardCount={setCardCount} />
          {cardData && cardCount === 9 && (
            <div className="nine-card-container">
              {cardData.map((card, idx) => (
                <div key={idx}>
                  <SmallCard card={card} />
                </div>
              ))}
            </div>
          )}
          {cardData.length === 3 && cardCount === 3 && (
            <div>
              <LargeCard cards={cardData} />
            </div>
          )}
          {cardData.length === 4 && cardCount === 4 && (
            <div className="four-card-container">
              <SmallCard card={cardData[0]} />
              <SmallCard card={cardData[1]} />
              <MediumCard cards={cardData.slice(2)} />
            </div>
          )}
          {cardData.length === 5 && cardCount === 5 && (
            <div className="five-card-container">
              <MediumCard cards={cardData.slice(0, 2)} />
              <LargeCard cards={cardData.slice(2)} />
            </div>
          )}
          {cardData.length === 6 && cardCount === 6 && (
            <div>
              <SmallCard card={cardData[0]} />
              <MediumCard cards={cardData.slice(1, 3)} />
              <LargeCard cards={cardData.slice(3)} />
            </div>
          )}
          {cardData.length === 7 && cardCount === 7 && (
            <div>
              <MediumCard cards={cardData.slice(0, 2)} />
              <MediumCard cards={cardData.slice(2, 4)} />
              <LargeCard cards={cardData.slice(4)} />
            </div>
          )}
          {cardData.length === 8 && cardCount === 8 && (
            <div className="eight-card-container">
              <div className="inner-small-container">
                <SmallCard card={cardData[0]} />
                <SmallCard card={cardData[1]} />
              </div>
              <MediumCard cards={cardData.slice(2, 4)} />
              <MediumCard cards={cardData.slice(4, 6)} />
              <MediumCard cards={cardData.slice(6)} />
            </div>
          )}
        </div>
      ) : (
        <SocialSecurityCard />
      )}
    </div>
  );
}
